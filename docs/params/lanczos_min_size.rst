*lanczos_min_size*
======================

Syntax
""""""

.. parsed-literal::

   lanczos_min_size = arg

* arg = integer


Default
"""""""

.. code-block:: bash

   lanczos_max_size = 0


Description
"""""""""""

Enforce Lanczos to always do at least this number of iterations. 


Unexpected behavior
"""""""""""""""""""


Related commands
""""""""""""""""

:doc:`lanczos_eval_conv_thr`, :doc:`lanczos_disp`, :doc:`lanczos_max_size`
